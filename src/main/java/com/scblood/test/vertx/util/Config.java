package com.scblood.test.vertx.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class Config {
	private final Properties prop = new Properties();

	private static Config instance = null;

	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		return instance;
	}

	private Config() {
		setConfig();
	}

	public void setConfig() {
		try {
			loadConfiguration(new File(Constants.config).getAbsolutePath());
			Constants.vertxServerName = getString("VERTX_SERVER_NAME");
			Constants.vertxNetServerPort = getInt("VERTX_NETSERVER_PORT");
			Constants.vertxBusHostIp = getString("VERTX_BUS_HOST_IP");
			Constants.vertxBusHostPort = getInt("VERTX_BUS_HOST_PORT");
			Constants.igniteIpDiscoverRange = getString("IGNITE_IP_DISCOVER_RANGE");
			Constants.igniteDiscoverLocalPort = getInt("IGNITE_DISCOVER_LOCAL_PORT");
			Constants.igniteDiscoverLocalPortRange = getInt("IGNITE_DISCOVER_LOCAL_PORT_RANGE");
			Constants.igniteTcpCommPort = getInt("IGNITE_TCP_COMM_PORT");
			
			Constants.igniteGridMapName = getString("IGNITE_GRID_MAP_NAME");
			Constants.igniteCacheMode = getInt("IGNITE_CACHE_MODE");
			Constants.igniteCacheBackupSyncMode = getInt("IGNITE_CACHE_BACKUP_SYNC_MODE");
			Constants.igniteCacheBackupCount = getInt("IGNITE_CACHE_BACKUP_COUNT");
			
			Constants.igniteDataStorageUseMode = getInt("IGNITE_DATA_STORAGE_USE_MODE");
			Constants.igniteDataStorageRegionSize = getLong("IGNITE_DATA_STORAGE_REGION_SIZE");
			Constants.igniteDataStorageConcurrencyLevel = getInt("IGNITE_DATA_STORAGE_CONCURR_LEVEL");
			Constants.igniteDataStoragePageSize = getInt("IGNITE_DATA_STORAGE_PAGE_SIZE");
			Constants.igniteDataStorageWalMode = getInt("IGNITE_DATA_STORAGE_WAL_MODE");
			
		
			Constants.vertxCommonBusAddressName = getString("VERTX_COMMON_BUS_ADD_NAME");
			Constants.vertxAtomBusAddressName = getString("VERTX_ATOM_BUS_ADD_NAME");
		
		
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadConfiguration(String path) throws InvalidPropertiesFormatException, IOException {
		File file = new File(path);
		InputStream input = new FileInputStream(file);
		prop.loadFromXML(input);
	}

	private String getString(String key) {
		String propertiesValue = prop.getProperty(key);
		return propertiesValue != null ? propertiesValue : "";
	}

	private int getInt(String key) {
		String propertiesValue = prop.getProperty(key);
		return propertiesValue != null ? Integer.parseInt(propertiesValue) : -1;
	}

	private long getLong(String key) {
		String propertiesValue = prop.getProperty(key);
		return propertiesValue != null ? Long.parseLong(propertiesValue) : -1;
	}
}
