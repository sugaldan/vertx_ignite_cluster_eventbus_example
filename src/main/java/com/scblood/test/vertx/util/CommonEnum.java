package com.scblood.test.vertx.util;

public class CommonEnum {
	
	public enum EventBusType {
		ALL,PUBLISH,ATOM;
	}
	
	public enum IgniteWalMode {
		FULLSYNC,LOGONLY,BACKGROUND,NONE;
	}	
	public enum IgniteCacheMode {
		REPLICATED,PARTITIONED,LOCAL;
	}	
	public enum IgniteCacheBackupSync {
		FULL_SYNC,FULL_ASYNC,PRIMARY_SYNC;
	}	
	
}
