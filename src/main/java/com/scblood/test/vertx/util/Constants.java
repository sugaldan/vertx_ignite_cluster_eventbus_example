package com.scblood.test.vertx.util;

import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.transactions.Transaction;

public class Constants {
	public static final String config ="conf/config_final.xml";
	
	/*
	 * 1. vertx에서 쓰는 서버 이름(로그 구분용)
	 * 2. vertx에서 tcp서버 쓸때 쓸 포트
	 * 3. vertx에서 event bus를 cluster로 할때 쓸 호스트 IP(자기 IP)-ignite설정과는 호환되지 않아 별도 설정해야한다.
	 * 4. vertx에서 event bus를 cluster로 할때 쓸 호스트 port(자기PORT)
	 */
	public static String vertxServerName ="HOSTNAME";
	public static int vertxNetServerPort =10000;
	public static String vertxBusHostIp ="127.0.0.1";
	public static int vertxBusHostPort =33333;
	
	/*
	 * 1. ignite를 검색할 IP 범위 xx.x.x.x,y.y.y.y 식으로 여러개 IP를 입력-자기 IP도 포함시켜야 함
	 * 2. ignite에서 서버 통신용 서버 open시 쓸 port
	 * 3. ignite에서 쓸 localport를 검색할 범위 20이면 48500->48520
	 * 4. ignite에서 쓸 tcp 통신 포트
	 * 5. ignite 사용시 사용할 공유 MAP 이름. 다하고싶으면 *
	 * 6. ignite 캐시 모드 선택. REPLICATED, PARTITIONED, LOCAL등이 있음 각각 0 1 2
	 * 7. ignite 캐시 백업 동기화모드 선택. FULL_SYNC,FULL_ASYNC,PRIMARY_SYNC 각각 0 1 2
	 * 8. ignite 캐시 백업 갯수 
	 */
	public static String igniteIpDiscoverRange ="127.0.0.1";
	public static int igniteDiscoverLocalPort =48500;
	public static int igniteDiscoverLocalPortRange =20;
	public static int igniteTcpCommPort =48100;	
	public static String igniteGridMapName="";
	public static int igniteCacheMode;
	public static int igniteCacheBackupSyncMode;
	public static int igniteCacheBackupCount;

	/*
	 * 1. ignite 공유 map 파일 저장 모드 사용 유무 0미사용 1 사용
	 * 2. ignite 저장공간 메모리 크기 단위 MB 
	 * 3. ignite cpu사용 갯수
	 * 4. ignite page 크기 단위 KB
	 * 5. ignite 파일저장 모드 DEFAULT(FULL_SYNC), LOG_ONLY, BACKGROUND,NONE 각각 0,1,2,3
	 */
	    
	public static int igniteDataStorageUseMode;
	public static long igniteDataStorageRegionSize;
	public static int igniteDataStorageConcurrencyLevel;
	public static int igniteDataStoragePageSize;
	public static int igniteDataStorageWalMode=1;
	
	/*
	 * Event Bus사용할건데 이에 관한 공통 수신 ADDRESS(이름)
	 * Event Bus 고유 수신 ADDRESS  
	 */
	public static String vertxCommonBusAddressName;
	public static String vertxAtomBusAddressName;
	
}
