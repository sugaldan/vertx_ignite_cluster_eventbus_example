package com.scblood.test.vertx.part.event.clusterbus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteLogger;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.WALMode;
import org.apache.ignite.logger.log4j.Log4JLogger;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.spi.cluster.ignite.IgniteClusterManager;

public class EventBusSenderMain {
	public EventBusSenderMain() {
	}
	
	public void deployMsg(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
	
	}
	private  Properties properties = new Properties();
	private void loadConfiguration(String path) throws InvalidPropertiesFormatException, IOException {
		File file = new File(path);
		InputStream input = new FileInputStream(file);
		properties.loadFromXML(input);
}
	private String getString(String key) {
		String propertiesValue = properties.getProperty(key);
		if (propertiesValue != null)
			return propertiesValue;
		else
			return "";
	}

	private String serverName ;
	private int port=0;
	private String discoverRange;
	private int discoverLocalPort;
	private int discoverLocalRange;
	private int tcpCommPort;
	private String busHost;
	private int busHostPort;
	
	public void init(){
		try {
			loadConfiguration(new File("config.xml").getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		serverName = getString("HOSTNAME");
		port = Integer.parseInt(getString("PORT"));
		discoverRange = getString("DISCOVER_RANGE");
		discoverLocalPort = Integer.parseInt(getString("DISCOVERY_LOCALPORT"));
		discoverLocalRange = Integer.parseInt(getString("DISCOVERY_LOCALPORTRANGE"));
		tcpCommPort = Integer.parseInt(getString("TCP_COMM_PORT"));
		busHost = getString("BUS_HOST");
		busHostPort = Integer.parseInt(getString("BUS_HOST_PORT"));
	}
	
	

	public IgniteClusterManager config(){
		//설정
				IgniteConfiguration cfg =   new IgniteConfiguration();
				IgniteLogger log;
				try {
					log = new Log4JLogger("log4j.xml");
					cfg.setGridLogger(log);					
				} catch (IgniteCheckedException e) {
					
					e.printStackTrace();
				}
				//TCP 조회하는 범위 설정
				TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
				//로컬 포트와, 로컬포트 할당 범위를 설정한다. 45800 20 하면 45800~45820까지 쓰겠단 소리다.
				discoverySpi.setLocalPort(discoverLocalPort);
				discoverySpi.setLocalPortRange(discoverLocalRange);
				
				//외부를 뒤질 IP와 포트설정인데, 나는 고정으로 썼다. 그리고 자기 IP도 설정을 해줘야 한다고 한다.192.168.1.108,192.168.1.120 식.	여기서도 위처럼 48500..48520식도 가능	
				TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
				ipFinder.setAddresses(Arrays.asList(discoverRange.split(",")));
				discoverySpi.setIpFinder(ipFinder);
				
				//Verx가 사용하는 TCP포트를 설정한다.
				TcpCommunicationSpi commSpi = new TcpCommunicationSpi();
				commSpi.setLocalPort(tcpCommPort);
				cfg.setDiscoverySpi(discoverySpi);
				cfg.setCommunicationSpi(commSpi);

				//메모리 그리드의 설정을 한다. 우선 사용할 MAP의 이름을 정한다. 
				CacheConfiguration cacheCfg = new CacheConfiguration("clusterDemo");
				//파티션 모드로 쓸 것인지. 완전복제(CacheMode.REPLICATED) 모드로 쓸것인지 결정한다.
				cacheCfg.setCacheMode(CacheMode.PARTITIONED);
				cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_ASYNC); //백업할때의 이야기 싱크설정
				//백업갯수를 몇개 만들것인지.
				cacheCfg.setBackups(1);
				cfg.setCacheConfiguration(cacheCfg); 
			
				IgniteClusterManager clusterManager = new IgniteClusterManager(cfg);
				return clusterManager;
	}
	public void eventBusSenderDeploy() {
		init();
		IgniteClusterManager mgr = config();
		
		VertxOptions option = new VertxOptions();
//		EventBusOptions eventBus = new EventBusOptions();
//		eventBus.setClustered(true);
//		
//		option.setEventBusOptions(eventBus);	
		option.setClustered(true);
		option.setClusterManager(mgr);		
		
		option.setClusterHost(busHost);
		option.setClusterPort(busHostPort);
		
		Vertx.clusteredVertx(option, res -> {
		       if (res.succeeded()) {
		    	   Vertx vertx = res.result();;
		    	   vertx.deployVerticle(new EventBusServer("SERVER_SENDER_10000"), asyncResult -> {
		   			deployMsg(asyncResult);
		   			new Thread(new TestThread(vertx)).start();
			   		});
		    	   
		       } else {
		         res.cause().printStackTrace();
		         System.out.println("OUT");
		       }
		   });
		
		
	}
	
	class TestThread implements Runnable{
		Vertx vertx;
		public TestThread(Vertx vertx){
			this.vertx = vertx;
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("RUN!!!!!!");
			vertx.eventBus().publish("Event_Address", "message 2");
	        vertx.eventBus().send   ("Event_Address", "message 1");
		}
		
	}
	public static void main(String[] args) {
		new EventBusSenderMain().eventBusSenderDeploy();
	}
}
