package com.scblood.test.vertx.part.event.clusterbus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;


/**
 * 이벤트 버스 수신 서버. 
 * 이벤트와 타임등을 송수신하는 부분
 * vertx는 외부에서 탑재해서 올라갈 거라 여기는 생성안함.
 * JDK 8기준
 * @author scblood.egloos.com
 *
 */
public class EventBusServer extends AbstractVerticle  {
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	
	private NetServer m_server;
	private String m_serverName;
	
	public EventBusServer(String name){
		m_serverName = name;
	}
	
	public void setEvent(){
		  vertx.eventBus().consumer("Event_Address", message -> {
	            System.out.println("\n\n["+m_serverName+"] Recieve Event" +message.body());
	        });
//		  
		  vertx.eventBus().consumer(m_serverName, message -> {
	            System.out.println("["+m_serverName+"] Recieve Event" +message.body());
	        });
	}
	
	
	/*
	 * 실제 동작 시키는 부분 위 메서드를 갖다 써도 무방 
	 * @see io.vertx.core.AbstractVerticle#start()
	 */
	@Override
	public void start() throws Exception {
		setEvent();
	}
	public void sendEventMsg() {
	    	vertx.eventBus().publish("Event_Address", "message 2");
	        vertx.eventBus().send   ("Event_Address", "message 1");    	        
	}
	

	@Override
	public void stop() {
		m_server.close(asyncResult -> {
			if (asyncResult.succeeded()) {
				System.out.println(m_serverName + " NetServer close");
			}
		});
		/*
		 * 시스템 완전종료를 위함 이거 안닫으면 자동 exit가 안됨
		 */
		vertx.close();
		System.out.println(m_serverName + " vertx close");
	}
	
}
