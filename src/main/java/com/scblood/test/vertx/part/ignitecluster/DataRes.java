package com.scblood.test.vertx.part.ignitecluster;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class DataRes {

	@JsonProperty("res_code")
	@SerializedName("res_code")	
	private int resCd;
	
	@JsonProperty("res_msg")
	@SerializedName("res_msg")		
	private String resMsg;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("data")
	private Object data;
}
