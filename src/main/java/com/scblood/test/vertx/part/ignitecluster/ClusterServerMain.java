package com.scblood.test.vertx.part.ignitecluster;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.WALMode;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.net.NetServer;
import io.vertx.spi.cluster.ignite.IgniteClusterManager;

public class ClusterServerMain extends AbstractVerticle{
	private static final Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	NetServer server;
	private String serverName ;
	private int port=0;
	private String discoverRange;
	private int discoverLocalPort;
	private int discoverLocalRange;
	private int tcpCommPort;
	
	IgniteClusterManager clusterManager ;
	
	
	public ClusterServerMain() throws MalformedURLException, IgniteCheckedException{
		init();
		//설정
		IgniteConfiguration cfg =   new IgniteConfiguration();
		//TCP 조회하는 범위 설정
		TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
		//로컬 포트와, 로컬포트 할당 범위를 설정한다. 45800 20 하면 45800~45820까지 쓰겠단 소리다.
		discoverySpi.setLocalPort(discoverLocalPort);
		discoverySpi.setLocalPortRange(discoverLocalRange);
		
		//외부를 뒤질 IP와 포트설정인데, 나는 고정으로 썼다. 그리고 자기 IP도 설정을 해줘야 한다고 한다.192.168.1.108,192.168.1.120 식.	여기서도 위처럼 48500..48520식도 가능	
		TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
		ipFinder.setAddresses(Arrays.asList(discoverRange.split(",")));
		discoverySpi.setIpFinder(ipFinder);
		
		//Verx가 사용하는 TCP포트를 설정한다.
		TcpCommunicationSpi commSpi = new TcpCommunicationSpi();
		commSpi.setLocalPort(tcpCommPort);
		cfg.setDiscoverySpi(discoverySpi);
		cfg.setCommunicationSpi(commSpi);

		//메모리 그리드의 설정을 한다. 우선 사용할 MAP의 이름을 정한다. 
		CacheConfiguration cacheCfg = new CacheConfiguration("clusterDemo");
		//파티션 모드로 쓸 것인지. 완전복제(CacheMode.REPLICATED) 모드로 쓸것인지 결정한다.
		cacheCfg.setCacheMode(CacheMode.PARTITIONED);
		cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_ASYNC); //백업할때의 이야기 싱크설정
		//백업갯수를 몇개 만들것인지.
		cacheCfg.setBackups(1);
		cfg.setCacheConfiguration(cacheCfg); 
		
		// 메모리 초기 설정모드로 용량을 제한거는거다. 
		DataStorageConfiguration psCfg = new DataStorageConfiguration();
		psCfg.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
		
		psCfg.getDefaultDataRegionConfiguration().setMaxSize(4L * 1024 * 1024 * 1024); //4G메모리 사용
		psCfg.setConcurrencyLevel(5); //cpu 몇개쓸것인가.
		psCfg.setPageSize(1024*4); //페이징 크기
				
//		//이건 메모리상에만 넣으면 맛이갈 수 있으니 디스크에도 쓴다는 소리이다. 경로는 /tmp/ignite/work에 저장
		psCfg.setWalMode(WALMode.DEFAULT); //FULLSYNC, LOG_ONLY등으로 설정 가능
		cfg.setDataStorageConfiguration(psCfg);
		// 이거 할라면 active로 해야함
		Ignite ignite = Ignition.start(cfg);  //캐시를 위해서 이렇게 작업을 해야함.
		
		ignite.active(true);
		clusterManager = new IgniteClusterManager(ignite);
	} 
	

	public IgniteClusterManager getManager(){
		return clusterManager;
	}
		
	private  Properties properties = new Properties();
	private void loadConfiguration(String path) throws InvalidPropertiesFormatException, IOException {
		File file = new File(path);
		InputStream input = new FileInputStream(file);
		properties.loadFromXML(input);
}

	private String getString(String key) {
		String propertiesValue = properties.getProperty(key);
		if (propertiesValue != null)
			return propertiesValue;
		else
			return "";
	}
	
	public void init(){
		try {
			loadConfiguration(new File("conf/config.xml").getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		serverName = getString("HOSTNAME");
		port = Integer.parseInt(getString("PORT"));
		discoverRange = getString("DISCOVER_RANGE");
		discoverLocalPort = Integer.parseInt(getString("DISCOVERY_LOCALPORT"));
		discoverLocalRange = Integer.parseInt(getString("DISCOVERY_LOCALPORTRANGE"));
		tcpCommPort = Integer.parseInt(getString("TCP_COMM_PORT"));
	}
	
	
	@Override
	public void start() throws Exception {
		System.out.println("START "+serverName+" port "+port);
		server = vertx.createNetServer();
		
		setNetServer();
		server.listen(port);
		setEvent();
	}

	//안먹히느중
	public void setEvent(){
		  vertx.eventBus().consumer("Event_Address", message -> {
	            System.out.println("["+serverName+"] Recieve Event" +message.body());
	        });
		  
		  vertx.eventBus().consumer(serverName, message -> {
	            System.out.println("["+serverName+"] Recieve Event" +message.body());
	        });
		  
	}
	//안먹히느중
	public void sendEventMsg(String msg) {
		vertx.eventBus().publish("Event_Address", msg);

	}
	    
	  
	public void stop() {
		setServerClose();
	}

	public void setNetServer() {
		server.connectHandler(socket -> {
			socket.handler(buff -> {
				String key=null;
				DataReq bean = gson.fromJson(buff.getString(0, buff.length()), DataReq.class);
				System.out.println("[" + serverName + "]" + gson.toJson(bean));
				
				/*
				 *  이부분은 클러스터가 연결되어있을 경우. 모든 작업이 끝난다음 공유Map에 변경을 하게 되어있는것 같음
				 *  실제 단독동작시에는 맵에 넣고 바로 빼도 변경되어있지만,
				 *  클러스터시에는 입력하고 바로 빼도 이전데이터가 존재하며 다음호출때는 변경되어있는 것을 알 수 있다.
				 */
				if(bean.getData()!= null){
					Map map = clusterManager.getSyncMap("clusterDemo"); // shared distributed map
					map.put("data", bean.getData());
					key = bean.getData().toString(); //이벤트고 뭐고 기존에 받은걸로 처리하고 공유 맵에는 끝난다음 들어가기떄문에 여기서 도로 뽑는 헛짓거리를 하지 말것
				}
				

				if(key==null){
					Map map = clusterManager.getSyncMap("clusterDemo"); // shared distributed map
					System.out.println("[" + serverName + "]SHARE " + map.get("data"));
					if (map.get("data") != null) {
						key = map.get("data").toString();
					}
				}
				
				DataRes res = new DataRes();
				res.setResCd(200);
				res.setResMsg(serverName);
				res.setData(key);
				Buffer outBuffer = Buffer.buffer();
				outBuffer.appendString(gson.toJson(res));
				socket.write(outBuffer);
				sendEventMsg("[" + serverName + "] Event "+key);
			});
		});
	}

	public void setServerClose() {
		server.close(asyncResult -> {
			if (asyncResult.succeeded()) {
				System.out.println("CLOSE");
			}
		});
		//굳이 여기까지 종료할 필요는 없다.
		vertx.close();
	}
	
	public void runServer(){
		//옵션설정, 이건 이벤트버스를 해볼라고 하는데 지금 잘 안된다. 현재는 무의미함
		VertxOptions options = new VertxOptions();
		options.setClusterManager(getManager());
		options.setClustered(true);
		EventBusOptions eventBus = new EventBusOptions();
		eventBus.setClustered(true);
		options.setEventBusOptions(eventBus);		 
		
		//클러스터 동작
	    Vertx.clusteredVertx(options, res -> {
	      if (res.succeeded()) {	    		    	  
	        Vertx vertx = res.result();        
	        vertx.deployVerticle(this);
	        
	      } else {
	    	  System.out.println("ClusterFail");
	      }
	    });
		
	}
	 
	public static void main(String[] args) throws MalformedURLException, IgniteCheckedException {
		ClusterServerMain a = new ClusterServerMain();
		a.runServer();
	  }  
	
	
}
