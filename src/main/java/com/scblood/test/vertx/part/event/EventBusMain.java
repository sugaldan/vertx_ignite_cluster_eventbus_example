package com.scblood.test.vertx.part.event;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

public class EventBusMain {
	private EventBusClient client = null;
	public EventBusMain() {
	}
	
	public void deployMsg(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
	
	}

	public void deployMsg2(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
		client.sendEventMsg();
	}


	public void eventBusSenderDeploy() {
		Vertx vertx = Vertx.vertx();
		
		vertx.deployVerticle(new EventBusServer("SERVER_10000",10000), asyncResult -> {
			deployMsg(asyncResult);
		});
		vertx.deployVerticle(new EventBusServer("SERVER_10001",10001), asyncResult -> {
			deployMsg(asyncResult);
		});
		
		client=	new EventBusClient("Sender_10000","127.0.0.1",10000);		
		vertx.deployVerticle(client, asyncResult -> {
			deployMsg2(asyncResult);
		});
	}
	
	
	public static void main(String[] args) {
		new EventBusMain().eventBusSenderDeploy();
	}
}
