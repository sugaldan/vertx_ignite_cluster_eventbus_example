package com.scblood.test.vertx.part.event;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBusOptions;

public class EventBusClientMain {
	private EventBusClient client = null;
	public EventBusClientMain() {
	}
	
	public void deployMsg(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
	
	}

	public void deployMsg2(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
		client.sendEventMsg();
	}


	public void eventBusSenderDeploy() {
		
		VertxOptions option = new VertxOptions();
		option.setClustered(true);
		EventBusOptions eventBus = new EventBusOptions();
		option.setEventBusOptions(eventBus);	
		
		Vertx.clusteredVertx(option, res -> {
		       if (res.succeeded()) {
		    	   Vertx vertx = res.result();;
		    	   
		    	   client=	new EventBusClient("Sender_10000","192.168.1.50",10000);		
			   		vertx.deployVerticle(client, asyncResult -> {
			   			deployMsg2(asyncResult);
			   		});
			   		
		       } else {
		         res.cause().printStackTrace();
		       }
		   });
		
		
		
	}
	
	
	public static void main(String[] args) {
		new EventBusClientMain().eventBusSenderDeploy();
	}
}
