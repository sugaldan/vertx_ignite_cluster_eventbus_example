package com.scblood.test.vertx.part.cs;

import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.net.NetSocket;
import io.vertx.core.streams.ReadStream;
import io.vertx.core.streams.WriteStream;


public class VertXSampleClient extends AbstractVerticle{
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	
	private NetClient m_client;
	private NetSocket m_netSocket;
	private int m_port =10000;
	private String m_ip;
	private boolean isConnect = false;
	public VertXSampleClient(String ip, int port){
		vertx = Vertx.vertx();
		m_ip = ip;
		m_port =port;
	}
	 
	@Override
	 public void start() throws Exception {
	    doConnnect();
	 }

	public void doConnnect(){
		NetClientOptions options = new NetClientOptions().setConnectTimeout(1000);
		setClientClose();
		
		m_client = vertx.createNetClient(options);
		m_client.connect(m_port, m_ip,asyncResult->{
			isConnect = asyncResult.succeeded();
			if(isConnect){
				System.out.println("connect");
				NetSocket socket = asyncResult.result();
				setConnectSocket(socket);
			}else{
				//fail
				System.out.println("connectFail");
				//뭔가 하면 됨 재접속 한다 던지 등
				
			}
		});
	}

	private void setConnectSocket(NetSocket socket) {
		/*
		 * Socket에서 BatchStream(ReadStream<Buffer> rs, WriteStream<Buffer> ws) 처럼 버퍼나, 핸들러만 따로 빼서 
		 * 클래스 만들수있기도 한데. 걍 현재 이렇게 해놓음. 굳이 그럴필요있나....
		 */
		
		m_netSocket = socket;
		socket.closeHandler(v->{
			//뭔가하면됨
			System.out.println("Socket Closed");
			doConnnect();
		});

		m_netSocket.exceptionHandler(e->{
			//printLog
			e.printStackTrace();
		});
			
	}
	
	public void setSocketRead(){
		m_netSocket.handler(buffer->{
			 System.out.println("Received data: " + buffer.length());
             System.out.println(buffer.getString(0, buffer.length()));
        });
	}
	
	public void doSendMsg(){
		 DataReq req = new DataReq();
		 req.setReqType(1);
		 req.setUserId("test");
		 m_netSocket.write(GSON.toJson(req));	
		 setSocketRead();
	}
	
	
	public void doDemoStart(){
		doConnnect();
		while(!isConnect){
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}

		doSendMsg();
	}
	
	public void setClientClose(){
		if(m_netSocket!=null){
			m_netSocket.close();	
		}
		if(m_client!=null){
			m_client.close();	
		}
			
		
	}
	
	public void setVertxClose(){
		vertx.close();
	}
	
	
	

	public static void main(String[] args){
		VertXSampleClient a = new VertXSampleClient("127.0.0.1",10000);
		a.doDemoStart();
		
	}
}
