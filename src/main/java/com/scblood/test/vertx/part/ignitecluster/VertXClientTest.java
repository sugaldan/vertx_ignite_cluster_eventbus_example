package com.scblood.test.vertx.part.ignitecluster;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;

//http://tutorials.jenkov.com/vert.x/tcp-client.html
public class VertXClientTest extends AbstractVerticle{
	private static final Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	private Vertx vertx;
	private NetClient tcpClient;
	public VertXClientTest(){
		vertx = Vertx.vertx();
	}

	private String ip="192.168.1.120";
//	private String ip="127.0.0.1";
	public void start() {
        tcpClient = vertx.createNetClient();
        tcpClient.connect(10010, ip,
                new Handler<AsyncResult<NetSocket>>(){

                @Override
                public void handle(AsyncResult<NetSocket> result) {
                    NetSocket socket = result.result();
                    DataReq bean = new DataReq();
                    bean.setReqType(1);
                    bean.setUserId("A");
                    bean.setData("ABCEDE");//이 부분이 주석이면  단순 공유맵에서 데이터를 뽑아준다.
                    socket.write(gson.toJson(bean));
                    
                    socket.handler(new Handler<Buffer>(){
                        @Override
                        public void handle(Buffer buffer) {
                            System.out.println("Received data: " + buffer.length());

                            System.out.println(buffer.getString(0, buffer.length()));
                            stop();
                        }
                    });
                }
            });
    }
	
	
	public void stop(){
		tcpClient.close();
		vertx.close();
	}
	public static void main(String[] args){
		VertXClientTest a = new VertXClientTest();
		a.start();
		
	}
}
