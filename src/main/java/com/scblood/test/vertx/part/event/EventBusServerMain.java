package com.scblood.test.vertx.part.event;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class EventBusServerMain {
	private EventBusClient client = null;
	public EventBusServerMain() {
	}
	
	public void deployMsg(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());		
	
	}
	



	public void eventBusSenderDeploy() {
		VertxOptions option = new VertxOptions();
		option.setClustered(true);

		Vertx.clusteredVertx(option, res -> {
		       if (res.succeeded()) {
		    	   Vertx vertx = res.result();;
		    	   
		    	   vertx.deployVerticle(new EventBusServer("SERVER_10000",10000), asyncResult -> {
		   			deployMsg(asyncResult);
			   		});
			   		vertx.deployVerticle(new EventBusServer("SERVER_10001",10001), asyncResult -> {
			   			deployMsg(asyncResult);
			   		});
			   		
		       } else {
		         res.cause().printStackTrace();
		       }
		   });
		
		
	}
	
	
	public static void main(String[] args) {
		new EventBusServerMain().eventBusSenderDeploy();
	}
}
