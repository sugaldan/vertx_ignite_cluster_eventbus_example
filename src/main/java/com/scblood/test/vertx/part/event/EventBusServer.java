package com.scblood.test.vertx.part.event;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;


/**
 * 이벤트 버스 수신 서버. 
 * 이벤트와 타임등을 송수신하는 부분
 * vertx는 외부에서 탑재해서 올라갈 거라 여기는 생성안함.
 * JDK 8기준
 * @author scblood.egloos.com
 *
 */
public class EventBusServer extends AbstractVerticle  {
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	
	private NetServer m_server;
	private int m_port =10000;
	private String m_serverName;
	
	public EventBusServer(String name,int port){
		this.m_port = port;
		m_serverName = name;
	}
	
	public void setPriodic(){
		long timerID = vertx.setPeriodic(3000, timerTime-> {
		        System.out.println("Timer 1 fired "+timerTime);
		});
		System.out.println(timerID);
	}
	public void setTimer(){
		long timerID = vertx.setTimer(3000, timerTime-> {
		        System.out.println("Timer fired "+timerTime);
		});
		System.out.println(timerID);
	}
	
	public void setEvent(){
		  vertx.eventBus().consumer("Event_Address", message -> {
	            System.out.println("["+m_serverName+"] Recieve Event" +message.body());
	        });
		  
		  vertx.eventBus().consumer(m_serverName, message -> {
	            System.out.println("["+m_serverName+"] Recieve Event" +message.body());
	        });
		  
	}
	
	
	/*
	 * 실제 동작 시키는 부분 위 메서드를 갖다 써도 무방 
	 * @see io.vertx.core.AbstractVerticle#start()
	 */
	@Override
	public void start() throws Exception {
		System.out.println(m_serverName+" START");
		m_server = vertx.createNetServer();
		setNetServer();
		m_server.listen(m_port);
		setEvent();
	}
	public void sendEventMsg() {
	    	vertx.eventBus().publish("Event_Address", "message 2");
	        vertx.eventBus().send   ("Event_Address", "message 1");    	        
	}
	
	/*
	 * 여기서 뭔가 수신받아 처리하고 관련 리턴 해주면 됨
	 */
	public DataRes doSomething(DataReq bean){
		DataRes res = new DataRes();
		res.setResCd(200);
		res.setResMsg("OK");
		res.setData("200 OK Data");
		return res;
	}
	
	/*
	 * 데이터 수신받고 전송하는 부분
	 */
	public void setNetServer() {
		m_server.connectHandler(socket -> {
			System.out.println("["+m_serverName+"] serverConnect");	
			socket.closeHandler(v->{
				System.out.println("Socket Closed");
			});
			
			
			socket.exceptionHandler(e->{
				//printLog
				e.printStackTrace();
			});
			
			socket.handler(buff -> {
				
				DataReq bean = GSON.fromJson(buff.getString(0, buff.length()), DataReq.class);
				System.out.println("Recieve Data \n"+GSON.toJson(bean));
				Buffer outBuffer = Buffer.buffer();
				
				DataRes res = doSomething(bean);
				
				outBuffer.appendString(GSON.toJson(res));
				socket.write(outBuffer);				
			});
		});
	}
	
	@Override
	public void stop() {
		m_server.close(asyncResult -> {
			if (asyncResult.succeeded()) {
				System.out.println(m_serverName + " NetServer close");
			}
		});
		/*
		 * 시스템 완전종료를 위함 이거 안닫으면 자동 exit가 안됨
		 */
		vertx.close();
		System.out.println(m_serverName + " vertx close");
	}
	
}
