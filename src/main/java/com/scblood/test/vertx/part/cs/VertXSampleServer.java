package com.scblood.test.vertx.part.cs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;


/**
 * Vert.x TCP 서버
 * 각 이벤트를 받아서 응답을 넘긴다.
 * JSON기준으로 만들어 넘길것.
 * JDK 8기준
 * http://tutorials.jenkov.com/vert.x/tcp-server.html
 * 
 * @author scblood.egloos.com
 *
 */
public class VertXSampleServer extends AbstractVerticle  {
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	
	private NetServer m_server;
	private int m_port =10000;
	private String m_serverName;
	
	public VertXSampleServer(String name,int port){
		/**
		 * VertxOptions 이 꽤 많이 있는데 걍 무시함. 알아서 찾아볼것
		 */
		vertx = Vertx.vertx();
		this.m_port = port;
		m_serverName = name;
	}
	
	/*
	 * 굳이 안써도 됨. 비동기 응답받고 싶다면 써도 되고. 어차피 start호출하기때문에 오버라이딩 할 필요없음
	 * @see io.vertx.core.AbstractVerticle#start(io.vertx.core.Future)
	 */
	/*@Override
	 public void start(Future<Void> startFuture) throws Exception{
		start();
	    startFuture.complete();
    }*/
	
	
	/*
	 * 실제 동작 시키는 부분 위 메서드를 갖다 써도 무방 
	 * @see io.vertx.core.AbstractVerticle#start()
	 */
	@Override
	public void start() throws Exception {
		System.out.println(m_serverName+" START");
		m_server = vertx.createNetServer();
		setNetServer();
		m_server.listen(m_port);
	}
	  
	
	/*
	 * 여기서 뭔가 수신받아 처리하고 관련 리턴 해주면 됨
	 */
	public DataRes doSomething(DataReq bean){
		DataRes res = new DataRes();
		res.setResCd(200);
		res.setResMsg("OK");
		res.setData("200 OK Data");
		return res;
	}
	
	/*
	 * 데이터 수신받고 전송하는 부분
	 */
	public void setNetServer() {
		m_server.connectHandler(socket -> {
						
			socket.closeHandler(v->{
				System.out.println("Socket Closed");
			});
			
			
			socket.exceptionHandler(e->{
				//printLog
				e.printStackTrace();
			});
			
			socket.handler(buff -> {
				
				DataReq bean = GSON.fromJson(buff.getString(0, buff.length()), DataReq.class);
				System.out.println("Recieve Data \n"+GSON.toJson(bean));
				Buffer outBuffer = Buffer.buffer();
				
				DataRes res = doSomething(bean);
				
				outBuffer.appendString(GSON.toJson(res));
				socket.write(outBuffer);				
			});
		});
	}
	
	/**
	 * 람다 없는 버전
	 */
/*	public void setNetServer() {
		m_server.connectHandler(new Handler<NetSocket>() {
			@Override
			public void handle(NetSocket socket) {
				socket.closeHandler(new Handler<Void>() {
					@Override
					public void handle(Void aVoid) {
						System.out.println("Socket Closed");
						//뭔가 하면 됨
					}
				});
				socket.exceptionHandler(new Handler<Throwable>() {
					@Override
					public void handle(Throwable event) {
						event.printStackTrace();
					}
				});
				
				System.out.println("Incoming connection!");
				socket.handler(new Handler<Buffer>() {
					@Override
					public void handle(Buffer buff) {
						System.out.println("incoming data: " + buff.length());
						DataBean bean = GSON.fromJson(buff.getString(0, buff.length()), DataBean.class);
						System.out.println(GSON.toJson(bean));
						Buffer outBuffer = Buffer.buffer();
						outBuffer.appendString("RESPONSE DATA");
						socket.write(outBuffer);
					}
				});
			}
		});
	}*/
	
	@Override
	public void stop() {
		m_server.close(asyncResult -> {
			if (asyncResult.succeeded()) {
				System.out.println(m_serverName + " NetServer close");
			}
		});
		/*
		 * 시스템 완전종료를 위함 이거 안닫으면 자동 exit가 안됨
		 */
		vertx.close();
		System.out.println(m_serverName + " vertx close");
	}
	
	/**
	 * 람다 없는 버전
	 */
/*	@Override
	public void stop(){
		m_server.close(new Handler<AsyncResult<Void>>() {
            @Override
            public void handle(AsyncResult result) {
                if(result.succeeded()){
                    System.out.println(m_serverName + " NetServer close");
                }
            }
        });
		vertx.close();
		System.out.println(m_serverName + " vertx close");
	}*/
	
	public static void main(String[] args) throws Exception {
		VertXSampleServer test = new VertXSampleServer("VertxServer ", 10000);
		test.start();

	}
}
