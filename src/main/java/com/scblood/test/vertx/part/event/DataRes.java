package com.scblood.test.vertx.part.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class DataRes {

	@JsonProperty("res_code")
	@SerializedName("res_code")	
	private int resCd;
	
	@JsonProperty("res_msg")
	@SerializedName("res_msg")		
	private String resMsg;
	
	@JsonProperty("data")
	private Object data;
}
