package com.scblood.test.vertx.finalize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Request 데이터 빈
 * 롬복으로 세팅
 * @author scblood.egloos.com
 *
 */
@Data
public class DataReq {
	
	@JsonProperty("req_type")
	@SerializedName("req_type")		
	private int reqType;
	
	@JsonProperty("user_id")
	@SerializedName("user_id")		
	private String userId;
	

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("data")
	@SerializedName("data")		
	private Object data;
	
	@JsonProperty("ext_data")
	@SerializedName("ext_data")	
	@JsonInclude(Include.NON_NULL)
	private String extData;
	
}
