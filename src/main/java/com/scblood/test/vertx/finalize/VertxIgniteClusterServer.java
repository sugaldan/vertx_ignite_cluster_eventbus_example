package com.scblood.test.vertx.finalize;

import java.util.Arrays;
import java.util.Map;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.WALMode;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scblood.test.vertx.part.ignitecluster.DataReq;
import com.scblood.test.vertx.part.ignitecluster.DataRes;
import com.scblood.test.vertx.util.CommonEnum;
import com.scblood.test.vertx.util.Constants;
import com.scblood.test.vertx.util.CommonEnum.EventBusType;
import com.scblood.test.vertx.util.CommonEnum.IgniteCacheBackupSync;
import com.scblood.test.vertx.util.CommonEnum.IgniteCacheMode;
import com.scblood.test.vertx.util.CommonEnum.IgniteWalMode;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServer;
import io.vertx.spi.cluster.ignite.IgniteClusterManager;

public class VertxIgniteClusterServer  extends AbstractVerticle{
	private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	
	private String serverName;
	private int serverPort = -1;
	private boolean isServerModeOn = false;
	private IgniteClusterManager clusterManager ;
	private NetServer netServer;
	/*
	 * 서버명, 서버포트, TCP서버를 켤 것인지 여부
	 */
	public VertxIgniteClusterServer(String name,int port, boolean tcpServerMode){
		serverName = name;
		this.serverPort = port;
		isServerModeOn = tcpServerMode;
		setConfiguration();
	}

	
	/*
	 * 실제 동작 시키는 부분 위 메서드를 갖다 써도 무방 
	 * @see io.vertx.core.AbstractVerticle#start()
	 * 이벤트 등록
	 */
	@Override
	public void start() throws Exception {
		System.out.println("Vertx Start");
		setReceiveEvent();
		if(isServerModeOn){
			netServer = vertx.createNetServer();
			setNetServer();
			netServer.listen(serverPort);		
			System.out.println("Server Run OK");
		}
	}
	
	/*
	 * 종료하는 부분인데 이부분에 종료하면 서버로서 기능 종료됨.
	 * @see io.vertx.core.AbstractVerticle#stop()
	 */
	@Override
	public void stop() {
		if(isServerModeOn){
			netServer.close(asyncResult -> {
				if (asyncResult.succeeded()) {
					System.out.println(serverName + " NetServer close");
				}
			});
		}

		
		
		/*
		 * 시스템 완전종료를 위함 이거 안닫으면 자동 exit가 안됨
		 */
		vertx.close();
		System.out.println(serverName + " vertx close");
	}
	
	
	/*
	 * Event Bus 수신 설정 
	 * 공통 Address 수신, 고유 Address수신함.
	 * 수신해서는 이후는 알아서.
	 */
	public void setReceiveEvent(){
		  vertx.eventBus().consumer(Constants.vertxCommonBusAddressName, message -> {
	            System.out.println("\n\n["+serverName+"] "+Constants.vertxCommonBusAddressName+"Recieve Event" +message.body());
	        });
		  vertx.eventBus().consumer(Constants.vertxAtomBusAddressName, message -> {
	            System.out.println("["+serverName+"] "+Constants.vertxAtomBusAddressName+" Recieve Event" +message.body());
	        });
	}
	/*
	 * 이벤트 전송 기능, 전체 다 전송하던지 특정 이름만 전송하던지.
	 */
	public void sendEventMsg(CommonEnum.EventBusType eventType,String atomBusName, String msg) {

		switch (eventType) {
		case ALL:
			vertx.eventBus().publish(Constants.vertxCommonBusAddressName, msg);
			vertx.eventBus().publish(atomBusName, msg);
			break;
		case PUBLISH:
			vertx.eventBus().publish(atomBusName, msg);
			break;
		case ATOM:
			vertx.eventBus().publish(atomBusName, msg);
			break;
		}
	}
	
	
	private void setNetServer() {
		netServer.connectHandler(socket -> {
			socket.handler(buff -> {
				String key=null;
				DataReq bean = GSON.fromJson(buff.getString(0, buff.length()), DataReq.class);
				System.out.println("[" + serverName + "]" + GSON.toJson(bean));
				
				/*
				 *  이부분은 클러스터가 연결되어있을 경우. 모든 작업이 끝난다음 공유Map에 변경을 하게 되어있는것 같음
				 *  실제 단독동작시에는 맵에 넣고 바로 빼도 변경되어있지만,
				 *  클러스터시에는 입력하고 바로 빼도 이전데이터가 존재하며 다음호출때는 변경되어있는 것을 알 수 있다.
				 */
				if(bean.getData()!= null){
					Map map = clusterManager.getSyncMap(Constants.igniteGridMapName); // shared distributed map
					map.put("data", bean.getData());
					key = bean.getData().toString(); //이벤트고 뭐고 기존에 받은걸로 처리하고 공유 맵에는 끝난다음 들어가기떄문에 여기서 도로 뽑는 헛짓거리를 하지 말것
				}

				//Event Bus를 쏘게 함
				sendEventMsg(EventBusType.ALL, "", "["+serverName+"] Event Send Recieve Data "+key );
				
				if(key==null){
					Map map = clusterManager.getSyncMap(Constants.igniteGridMapName); // shared distributed map
					System.out.println("[" + serverName + "]SHARE " + map.get("data"));
					if (map.get("data") != null) {
						key = map.get("data").toString();
					}
				}

				
				DataRes res = new DataRes();
				res.setResCd(200);
				res.setResMsg(serverName);
				res.setData(key);
				Buffer outBuffer = Buffer.buffer();
				outBuffer.appendString(GSON.toJson(res));
				socket.write(outBuffer);				
			});
		});
	}
	
	public IgniteClusterManager getClusterManager(){
		return clusterManager;
	}
	
	/*
	 * Ignite등 각종 설정을 한다. 
	 */
	private void setConfiguration(){
		
		IgniteConfiguration cfg =   new IgniteConfiguration();
		//TCP 조회하는 범위 설정
		TcpDiscoverySpi discoverySpi = new TcpDiscoverySpi();
		//로컬 포트와, 로컬포트 할당 범위를 설정한다. 45800 20 하면 45800~45820까지 쓰겠단 소리다.
		discoverySpi.setLocalPort(Constants.igniteDiscoverLocalPort);
		discoverySpi.setLocalPortRange(Constants.igniteDiscoverLocalPortRange);
		
		//외부를 뒤질 IP와 포트설정인데, 나는 고정으로 썼다. 그리고 자기 IP도 설정을 해줘야 한다고 한다.192.168.1.108,192.168.1.120 식.	여기서도 위처럼 48500..48520식도 가능	
		TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
		ipFinder.setAddresses(Arrays.asList(Constants.igniteIpDiscoverRange.split(",")));
		discoverySpi.setIpFinder(ipFinder);
		
		//Verx가 사용하는 TCP포트를 설정한다.
		TcpCommunicationSpi commSpi = new TcpCommunicationSpi();
		commSpi.setLocalPort(Constants.igniteTcpCommPort);
		cfg.setDiscoverySpi(discoverySpi);
		cfg.setCommunicationSpi(commSpi);

		//메모리 그리드의 설정을 한다. 우선 사용할 MAP의 이름을 정한다. 
		CacheConfiguration cacheCfg = new CacheConfiguration(Constants.igniteGridMapName);
		IgniteCacheMode cacheMode = IgniteCacheMode.values()[Constants.igniteCacheMode];
		//파티션 모드로 쓸 것인지. 완전복제(CacheMode.REPLICATED) 모드로 쓸것인지 결정한다.
		switch(cacheMode){
		case REPLICATED:
			cacheCfg.setCacheMode(CacheMode.REPLICATED);
			break;
		case PARTITIONED:
			cacheCfg.setCacheMode(CacheMode.PARTITIONED);
			break;
		case LOCAL:
			cacheCfg.setCacheMode(CacheMode.LOCAL);
			break;
		}
		
		IgniteCacheBackupSync backupSync = IgniteCacheBackupSync.values()[Constants.igniteCacheBackupSyncMode];
		switch(backupSync){
		case FULL_SYNC:
			cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC); //백업할때의 이야기 싱크설정
			break;
		case FULL_ASYNC:
			cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_ASYNC); 
			break;
		case PRIMARY_SYNC:
			cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.PRIMARY_SYNC); 
			break;
		}
		
		//백업갯수를 몇개 만들것인지.
		cacheCfg.setBackups(Constants.igniteCacheBackupCount);
		cfg.setCacheConfiguration(cacheCfg); 

		
		//메모리 그리드 설정 여부
		if(Constants.igniteDataStorageUseMode>0){
			// 메모리 초기 설정모드로 용량을 제한거는거다. 
			DataStorageConfiguration psCfg = new DataStorageConfiguration();
			psCfg.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
			
			psCfg.getDefaultDataRegionConfiguration().setMaxSize(Constants.igniteDataStorageRegionSize * 1024 * 1024); //메모리 사용 mb
			psCfg.setConcurrencyLevel(Constants.igniteDataStorageConcurrencyLevel); //cpu 몇개쓸것인가.
			psCfg.setPageSize(1024*Constants.igniteDataStoragePageSize); //페이징 크기 kb
					
			//이건 메모리상에만 넣으면 맛이갈 수 있으니 디스크에도 쓴다는 소리이다. 경로는 /tmp/ignite/work에 저장
			IgniteWalMode walMode = IgniteWalMode.values()[Constants.igniteDataStorageWalMode];
			switch(walMode){
			case FULLSYNC:
				psCfg.setWalMode(WALMode.DEFAULT); //FULLSYNC, LOG_ONLY등으로 설정 가능
				break;
			case LOGONLY:
				psCfg.setWalMode(WALMode.LOG_ONLY); 
				break;
			case BACKGROUND:
				psCfg.setWalMode(WALMode.BACKGROUND);
				break;
			case NONE:
				psCfg.setWalMode(WALMode.NONE);
				break;
			}
			
			cfg.setDataStorageConfiguration(psCfg);
			// 이거 할라면 active로 해야함
			Ignite ignite = Ignition.start(cfg);  //캐시를 위해서 이렇게 작업을 해야함.
			ignite.active(true);
			clusterManager = new IgniteClusterManager(ignite);
		}else{
			clusterManager = new IgniteClusterManager(cfg);
		}
	}
}
