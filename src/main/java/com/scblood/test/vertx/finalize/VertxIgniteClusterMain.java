package com.scblood.test.vertx.finalize;

import com.scblood.test.vertx.util.Config;
import com.scblood.test.vertx.util.Constants;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class VertxIgniteClusterMain {
	private VertxIgniteClusterServer server = null;
	public VertxIgniteClusterMain(){
		System.out.println("START");
		Config.getInstance();	
		doRunServer();
		
	}
	
	private void setResultMsg(AsyncResult<String> result){
		System.out.println("BasicVerticle deployment complete");
		System.out.println("success "+result.succeeded());			
	}
	
	private void doRunServer(){
		server = new VertxIgniteClusterServer(Constants.vertxServerName, Constants.vertxNetServerPort, true);
		VertxOptions option = new VertxOptions();
		option.setClustered(true);
		option.setClusterManager(server.getClusterManager());
		option.setClusterHost(Constants.vertxBusHostIp);
		option.setClusterPort(Constants.vertxBusHostPort);
		
		
		Vertx.clusteredVertx(option, res -> {
		       if (res.succeeded()) {
		    	   Vertx vertx = res.result();
		    	   System.out.println("Cluster OK");
		    	   vertx.deployVerticle(server, asyncResult -> {
		    		  setResultMsg(asyncResult);
			   		});
		       } else {
		         res.cause().printStackTrace();
		         System.out.println("Failed");
		       }
		   });
		
	}
	
		
	public static void main(String[] args){
		new VertxIgniteClusterMain();
	}
}
