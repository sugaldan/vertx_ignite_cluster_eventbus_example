#!/bin/bash

java -XX:+UseG1GC -Xmx4g -XX:MaxDirectMemorySize=4g -XX:+DisableExplicitGC -cp "/home/scblood/ignite/lib/*" com.scblood.test.vertx.event.clusterbus.EventBusServerMain &
